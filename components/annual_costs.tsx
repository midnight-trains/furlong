import * as React from 'react';
import _ from 'lodash';
import { fmt } from '../lib/helpers';
import VehicleJourney from '../lib/vehicle_journey';
import { Day } from '../lib/types.d';
import { mondays, yearCost } from '../lib/vj_helper';

type Props = {
  vjs: {
    [sens: string]: { [day: string]: VehicleJourney };
  };
  defaultOff: number;
};

const AnnualCosts: React.FunctionComponent<Props> = ({
  vjs,
  defaultOff,
}: Props) => {
  const [off, setOff] = React.useState(defaultOff);
  const selected = {
    [Day.Monday]: mondays(off),
    [Day.Friday]: 52,
    [Day.Saturday]: 52,
    [Day.Sunday]: 52,
  };

  const { aller, retour } = vjs;
  const energy =
    yearCost(aller, 'energy', off) + yearCost(retour, 'energy', off);
  const tracks =
    yearCost(aller, 'tracks', off) + yearCost(retour, 'tracks', off);
  const station =
    yearCost(aller, 'station', off) + yearCost(retour, 'station', off);

  const sum = energy + tracks + station;

  return (
    <div>
      <table>
        <tr>
          <td>Couts infrastructure</td>
          <td className="text-right">{`${fmt(sum / 1000)} k€`}</td>
        </tr>
        <tr>
          <td className="text-right text-xs py-1 ">
            Voies (redevance marché + circulation)
          </td>
          <td className="text-right text-xs py-1 ">
            {`${fmt(tracks / 1000)} k€`}
          </td>
        </tr>
        <tr>
          <td className="text-right text-xs py-1 ">
            Énergie (accès, distribution, fourniture)
          </td>
          <td className="text-right text-xs py-1 ">
            {`${fmt(energy / 1000)} k€`}
          </td>
        </tr>
        <tr>
          <td className="text-right text-xs py-1 ">
            Gares (redevance quai et gares)
          </td>
          <td className="text-right text-xs py-1 ">
            {`${fmt(station / 1000)} k€`}
          </td>
        </tr>
        <tr>
          <td>Indisponibilité (maintenance + aléas)</td>
          <td className="text-right">
            <label htmlFor="off">
              <input
                className="w-10 text-right"
                type="number"
                id="off"
                min="0"
                max="100"
                onChange={(x) => setOff(x.target.valueAsNumber)}
                value={off}
              />
              %
            </label>
          </td>
        </tr>
        <tr>
          <td>Nombre total d’aller-retours</td>
          <td className="text-right">{_(selected).values().sum()}</td>
        </tr>
        <tr>
          <td className="text-xs py-1 text-right">Aller-retours en semaine</td>
          <td className="text-xs py-1 text-right">{selected[Day.Monday]}</td>
        </tr>
        <tr>
          <td className="text-xs py-1 text-right">Aller-retours un vendredi</td>
          <td className="text-xs py-1 text-right">{selected[Day.Friday]}</td>
        </tr>
        <tr>
          <td className="text-xs py-1 text-right">Aller-retours un samedi</td>
          <td className="text-xs py-1 text-right">{selected[Day.Saturday]}</td>
        </tr>
        <tr>
          <td className="text-xs py-1 text-right">Aller-retours un dimanche</td>
          <td className="text-xs py-1 text-right">{selected[Day.Sunday]}</td>
        </tr>
      </table>
    </div>
  );
};

export default AnnualCosts;
